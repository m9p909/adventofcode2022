#include <bits/stdc++.h>
using namespace std;
int main() {
  string s;
  int total = 0;
  auto strToValue = [](char c) {
    int v = int(c);
    if (v >= 97 && v <= 122) {
      return v - 96;
    }

    if (v >= 65 && v <= 90) {
      return v - 65 + 27;
    }
    return -10000;
  };

  auto strContains = [](string s, char c) { return s.find(c) != string::npos; };

  vector<string> elves;
  while (getline(cin, s)) {
    elves.push_back(s);
    if (elves.size() >= 3) {
      set<char> badgeOptions;

      for (char v : elves[0]) {
        badgeOptions.emplace(v);
      }
      for (int i = 1; i < 3; i++) {
        set<char> todel;
        for (auto c : badgeOptions) {
          if (!strContains(elves[i], c)) {
            todel.emplace(c);
          }
        }
        for (auto c : todel) {
          badgeOptions.erase(c);
        }
      }

      if (badgeOptions.size() > 1) {
        cout << "more than 1 option error";
      }
      for (auto v : badgeOptions) {
        total += strToValue(v);
      }

      elves.clear();
    }
  }
  cout << total;

  // solution comes here
}
