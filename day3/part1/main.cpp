#include <bits/stdc++.h>
using namespace std;
int main() {
  string s;
  int total = 0;
  auto strToValue = [](char c) {
    int v = int(c);
    if (v >= 97 && v <= 122) {
      return v - 96;
    }

    if (v >= 65 && v <= 90) {
      return v - 65 + 27;
    }
    return -10000;
  };

  while (getline(cin, s)) {
    string s1 = s.substr(0, s.size() / 2);
    string s2 = s.substr(s.size() / 2, s.size() - 1);
    //    cout << "s: " << s << " s1: " << s1 << " s2: " << s2 << endl;
    set<char> duplicateChars;
    set<char> s1cs;
    for (char c : s1) {
      s1cs.emplace(c);
    }
    for (char c : s2) {
      if (s1cs.count(c)) {
        duplicateChars.emplace(c);
      }
    }

    for (char c : duplicateChars) {
      total += strToValue(c);
      cout << total << endl;
    }
  }
  cout << total;

  // solution comes here
}
