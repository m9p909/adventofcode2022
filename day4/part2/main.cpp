#include <bits/stdc++.h>
using namespace std;
int main() {
  string s;
  int total = 0;
  int a;
  int b;
  int c;
  int d;
  auto overlaps = [](int a, int b, int c, int d) { return a <= d && c <= b; };
  while (scanf("%d-%d,%d-%d", &a, &b, &c, &d) != EOF) {
    if (overlaps(a, b, c, d)) {
      total++;
      continue;
    }

    printf("%d-%d, %d-%d \n", a, b, c, d);
  }
  cout << total;

  // solution comes here
}
