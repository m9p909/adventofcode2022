#include <bits/stdc++.h>
using namespace std;
int main() {
  string s;
  int total = 0;
  int a;
  int b;
  int c;
  int d;
  auto contains = [](int a, int b, int c, int d) { return (a <= c && b >= d); };
  while (scanf("%d-%d,%d-%d", &a, &b, &c, &d) != EOF) {
    if (contains(a, b, c, d)) {
      total++;
      continue;
    }

    if (contains(c, d, a, b)) {
      total++;
      continue;
    }
  }
  cout << total;

  // solution comes here
}
