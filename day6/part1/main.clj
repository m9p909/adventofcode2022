
(def data (slurp "./input.in"))

(defn previous-3-have-no-matches [data x]
  (let [v (get data x)]
    (loop [i 1
           prev (set [v])]
      (if (contains? prev (get data (- x i))) ; x -1 x -2 x- 3
        false
        (if (< i 13)
          (recur (inc i) (conj prev (get data (- x i))))
          true)))))

(defn is-marker? [data x]
  (if (> x 13)
    (previous-3-have-no-matches data x)
    false))

(defn getSolution [data]
  (inc (loop [x 0]
         (if (is-marker? data x)
           x
           (recur (inc x))))))

(def tests ["bvwbjplbgvbhsrlpgdmjqwftvncz", "nppdvjthqldpwncqszvftbrmjlhg" "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"])



