#include <bits/stdc++.h>
using namespace std;

vector<stack<char>> parseStacks() {
  vector<vector<char>> stackmatrix1;
  string line;
  for (int i = 0; i < 8; i++) {
    getline(cin, line);
    vector<char> data;

    for (char c : line) {
      if (c != '[' && c != ']')
        data.push_back(c);
      else
        data.push_back(' ');
    }
    stackmatrix1.push_back(data);
  }

  vector<vector<char>> stackmatrix2;

  for (auto v : stackmatrix1) {
    vector<char> matrix;
    for (int i = 0; i < v.size(); i++) {
      if (!(i % 2 == 0)) {
        matrix.push_back(v[i]);
      }
    }
    stackmatrix2.push_back(matrix);
  }

  vector<stack<char>> output;

  for (int j = 0; j < stackmatrix2[0].size(); j++) {
    stack<char> s;
    for (int i = stackmatrix2.size() - 1; i >= 0; i--) {
      if (stackmatrix2[i][j] != ' ') {
        s.push(stackmatrix2[i][j]);
      }
    }
    if (s.size() > 0)
      output.push_back(s);
  }
  return output;
};

void runTheProcedure(vector<stack<char>> *stacks) {
  int amount;
  int from;
  int to;
  while (scanf("move %d from %d to %d \n", &amount, &from, &to) != EOF) {

    for (int i = 0; i < amount; i++) {
      stacks->at(to - 1).push(stacks->at(from - 1).top());
      stacks->at(from - 1).pop();
    }
  }
};

void outputTheTopOfTheStacks(vector<stack<char>> *stacks) {
  for (int i = 0; i < stacks->size(); i++) {
    cout << stacks->at(i).top();
  }
  cout << endl;
}

int main() {
  vector<stack<char>> stacks = parseStacks();
  string line;
  getline(cin, line);
  getline(cin, line);
  runTheProcedure(&stacks);

  outputTheTopOfTheStacks(&stacks);
}
