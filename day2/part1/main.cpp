#include <bits/stdc++.h>
using namespace std;
int main() {
  string s;
  auto beats = [](int ia, int ib) { // rock paper scissors
    // 0 1 -> -
    // 1 0 -> +
    // 0 2 -> +
    // 2 0 -> -
    // 1 2 -> -
    // 2 1 -> +
    // 0 0 -> 0

    int winning[][2] = {{1, 0}, {0, 2}, {2, 1}};
    if (ia == ib) {
      return 0;
    }
    int won = -1;
    for (auto v : winning) {
      if (v[0] == ia && v[1] == ib) {
        won = 1;
      }
    }
    return won;
  };

  vector<string> optionsA = {"A", "B", "C"};
  vector<string> optionsB = {"X", "Y", "Z"};

  auto getPoints = [optionsA, optionsB, beats](string p1, string p2) {
    int score = 0;
    int ia =
        distance(optionsA.begin(), find(optionsA.begin(), optionsA.end(), p1));
    auto ib =
        distance(optionsB.begin(), find(optionsB.begin(), optionsB.end(), p2));
    score += ib + 1;

    int won = beats(ia, ib);
    if (won == 0) {
      score += 3;
    }
    if (won == -1) {
      score += 6;
    }
    return score;
  };

  int score = 0;
  while (getline(cin, s)) {
    string p1;
    string p2;
    p1 = s.substr(0, s.find(' '));
    p2 = s.substr(s.find(' ') + 1, 3);
    cout << p1 << p2 << getPoints(p1, p2) << endl;
    score += getPoints(p1, p2);
  }

  cout << score;

  // solution comes here
}
