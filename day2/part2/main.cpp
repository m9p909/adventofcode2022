#include <asm-generic/errno-base.h>
#include <bits/stdc++.h>
using namespace std;
int main() {
  string s;
  auto getBonusPoints = [](int ia, int ib) { // rock paper scissors
    // 0 0 -> 3 2
    // 1 0 -> 1 0
    // 2 0 -> 2 1
    // 1 1 -> 1
    // 2 1 -> 2
    // 0 2 -> 2 1
    // 1 2 -> 3 2
    // 2 2 -> 1 0

    if (ib == 1) {
      return ia + 1;
    }
    if (ib == 0) {
      return ((ia + 2) % 3) + 1;
    }
    if (ib == 2) {
      return ((ia + 1) % 3) + 1;
    }
    return -2000;
  };

  // rock paper scissors
  vector<string> optionsA = {"A", "B", "C"};
  vector<string> optionsB = {"X", "Y", "Z"};
  // lose draw win

  auto getPoints = [optionsA, optionsB, getBonusPoints](string p1, string p2) {
    int score = 0;
    int ia =
        distance(optionsA.begin(), find(optionsA.begin(), optionsA.end(), p1));
    auto ib =
        distance(optionsB.begin(), find(optionsB.begin(), optionsB.end(), p2));
    score += getBonusPoints(ia, ib);

    int won = ib - 1;
    if (won == 0) {
      score += 3;
    }
    if (won == 1) {
      score += 6;
    }
    return score;
  };

  int score = 0;
  while (getline(cin, s)) {
    string p1;
    string p2;
    p1 = s.substr(0, s.find(' '));
    p2 = s.substr(s.find(' ') + 1, 3);
    cout << p1 << p2 << getPoints(p1, p2) << endl;
    score += getPoints(p1, p2);
  }

  cout << score;

  // solution comes here
}
