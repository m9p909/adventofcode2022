use std::io::{self, Write};

fn main() -> io::Result<()> {
    let mut buffer = String::new();
    let stdin = io::stdin(); // We get `Stdin` here.
    let out = stdin.read_line(&mut buffer)?;
    io::stdout().write_all(buffer.as_bytes());
    Ok(())
}
