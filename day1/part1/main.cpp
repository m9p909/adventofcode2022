#include <bits/stdc++.h>
using namespace std;
int main() {
  string s;
  int sum = 0;
  int max = 0;
  while (getline(cin, s)) {
    if (s.empty()) {
      if (sum > max) {
        max = sum;
      }
      sum = 0;
      continue;
    }

    int v = stoi(s);
    sum += v;
  }
  cout << max;

  // solution comes here
}
