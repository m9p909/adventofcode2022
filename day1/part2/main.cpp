#include <bits/stdc++.h>
using namespace std;
int main() {
  string s;
  int sum = 0;
  vector<int> maxValues = {0, 0, 0};
  while (getline(cin, s)) {
    if (s.empty()) {
      maxValues.push_back(sum);
      push_heap(maxValues.begin(), maxValues.end());
      sum = 0;
      continue;
    }

    int v = stoi(s);
    sum += v;
  }
  sum = 0;

  for (int i = 0; i < 3; i++) {
    pop_heap(maxValues.begin(), maxValues.end());
    int v = maxValues.back();
    maxValues.pop_back();
    sum += v;
    cout << v << endl;
  }
  cout << sum << endl;

  // solution comes here
}
